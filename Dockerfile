# Use an official Node.js runtime as the base image
FROM node

# Set the working directory inside the container
WORKDIR /app

# Copy the package.json and package-lock.json to the container
COPY package*.json ./

# Install project dependencies
RUN npm install

# Copy the entire project to the container
COPY . .

# Expose the development server port (usually 8080)
EXPOSE 8080

# Start the Vue.js development server
CMD ["npm", "run", "dev"]
