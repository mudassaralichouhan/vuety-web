import {defineStore, type StoreDefinition} from 'pinia'
import {reactive, readonly, watch} from "vue";
import {useCookies} from "vue3-cookies";
import CryptoJS from 'crypto-js';

export interface setupObjectStore {
  id: number
  name: string
  email: string
  email_verified_at: string | null
  photo: string | null
  updated_at: string
  created_at: string
}

interface setupInterface {
  token: string | null,
  refresh_token: string | null,
  setup: setupObjectStore | null,
}

interface useSessionStoreInterface {
  setToken: (token: string | null) => void
  getToken: () => string | null
  setRefreshTokenSession: (token: string) => void
  setSetup: (o: setupObjectStore) => void
  reset: () => void
  getSetup: () => setupInterface
}

export const useSessionStore = defineStore('session', (): useSessionStoreInterface => {
  const state: setupInterface = reactive({
    token: null,
    refresh_token: null,
    setup: null,
  });
  const {cookies} = useCookies();
  const sessionInStorage: string | null = localStorage.getItem('session');
  const COOKIE_KEY = 'authentication'
  const storeCookie = cookies.get(COOKIE_KEY)

  function setToken(token: string | null): void {
    state.token = token;
  }

  function getToken(): string | null {

    if (state.token && state.token != '')
      return state.token;

    return null;
  }

  function setRefreshTokenSession(token: string | null): void {
    state.refresh_token = token;
  }

  function setSetup(o: setupObjectStore | null): void {
    state.setup = o;
  }

  function reset(): void {
    state.token = null;
    state.refresh_token = null;
    state.setup = null;
  }

  function getSetup(): setupInterface {
    return readonly(state);
  }

  if (storeCookie && sessionInStorage) {

    const decrypt = CryptoJS.AES
      .decrypt(storeCookie, '123456')
      .toString(CryptoJS.enc.Utf8);

    const tokens = JSON.parse(decrypt);

    state.refresh_token = tokens.refresh_token;
    state.token = tokens.token;

    state.setup = JSON.parse(
      CryptoJS.AES
        .decrypt(sessionInStorage, '123456')
        .toString(CryptoJS.enc.Utf8)
    );

  } else {
    cookies.remove(COOKIE_KEY);
    window.localStorage.removeItem('session');
  }

  watch(() => state, (state: setupInterface) => {

    const encrypt = CryptoJS.AES
      .encrypt(JSON.stringify({
        token: state.token,
        refresh_token: state.refresh_token
      }), '123456')
      .toString();

    if (state.token?.length && state.refresh_token?.length) {
      cookies.set(COOKIE_KEY, encrypt);

      const encryptSetup = CryptoJS.AES
        .encrypt(JSON.stringify(state.setup), '123456')
        .toString();

      localStorage.setItem('session', encryptSetup);
    } else {
      cookies.remove(COOKIE_KEY);
      window.localStorage.removeItem('session');
    }

  }, {deep: true});

  return {
    setToken,
    getToken,
    setRefreshTokenSession,
    setSetup,
    reset,
    getSetup,
  }
},);
