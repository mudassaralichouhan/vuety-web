import {defineStore} from 'pinia';

import apiClient, {api} from "@/config/api/api";
import router from '@/router/index';
import {useSessionStore} from "@/stores/sessionStore"
import {toast} from "vue3-toastify";

interface useCustomerStoreInterface {
  login: (email: string, password: string) => Promise<void>,
  logout: () => Promise<void>,
}

export const useCustomerStore = defineStore("auth", (): useCustomerStoreInterface => {
  const $state = useSessionStore();

  async function login(email: string, password: string): Promise<void> {
    if ($state.getToken()) {
      await router.push({name: "customer-dashboard"});
      //throw Promise.reject('all-ready login')
    }

    await api.post('/auth/login', {
      email: email,
      password: password
    })
      .then((response) => {
        $state.setToken(response.data.token);
        $state.setRefreshTokenSession('============');
        $state.setSetup(response.data?.user || response.data);
        return response;
      })
      .catch(e => {
        throw e
      });
  }

  async function logout(): Promise<void> {
    await apiClient.post("/auth/logout").then(() => {
      $state.reset();
      router.push('/login');
    }).catch((e) => {
      toast(e.message, {
        autoClose: 3000,
      });
    });
  }

  return {
    login,
    logout,
  }
});