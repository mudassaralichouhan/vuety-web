import {defineStore} from 'pinia';

import apiClient, {api} from "@/config/api/api";
import router from '@/router/index';
import {useSessionStore} from "@/stores/sessionStore"
import {toast} from "vue3-toastify";

export const useCustomerStore = defineStore("auth", {
  state: () => ({
    session: useSessionStore(),
  }),
  actions: {
    async login(email: string, password: string): Promise<void> {
      if (this.session.token) {
        await router.push({name: "customer-dashboard"});
        //throw Promise.reject('all-ready login')
      }

      await api.post('/auth/login', {
        email: email,
        password: password
      })
        .then((response) => {
          this.session.setToken(response.data.token);
          this.session.setRefreshTokenSession('============');
          this.session.setSetup(response.data?.user || response.data);
          return response;
        })
        .catch(e => {
          throw e
        });
    },
    async logout(): Promise<void> {
      await apiClient.post("/auth/logout").then(() => {
        this.session.reset();
        router.push('/login');
      }).catch((e) => {
        toast(e.message, {
          autoClose: 3000,
        });
      });
    },
  }
});