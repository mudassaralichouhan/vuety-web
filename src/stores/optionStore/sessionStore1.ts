import {defineStore} from 'pinia'

export interface sessionStore {
	token: string | null,
	refresh_token: string | null,
	setup: Object,
}

export const useSessionStore = defineStore('authState', {
	state: (): sessionStore => {
		return {
			token: null,
			refresh_token: null,
			setup: {},
		}
	},
	persist: {
		storage: window.localStorage,
		paths: ['token', 'refresh_token', 'setup'],
	},
	getters: {
		getSetup(): sessionStore {
			return {
				token: this.token,
				refresh_token: this.refresh_token,
				setup: this.setup,
			};
		},
	},
	actions: {
		// any amount of arguments, return a promise or not
		setToken(token: string): void {
			this.token = token;
		},
		setRefreshTokenSession(token: string): void {
			this.refresh_token = token;
		},
		setSetup(s: Object) {
			this.setup = s;
		},
		reset(): void {
			this.token = null;
			this.refresh_token = null;
			this.setup = {};
		},
	},
});
