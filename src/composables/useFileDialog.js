import {ref} from "vue";

export function useFileDialog() {
    const inputRef = ref(null);
    const files = ref(null);

    const input = document.createElement("input");
    input.type = "file";
    input.hidden = true;
    input.className = "hidden";
    document.body.appendChild(input);
    inputRef.value = input;

    inputRef.value.onchange = (e) => {
        files.value = e.target.files;
    }

    function openDialog() {
        inputRef.value?.click();
    }

    return {
        openDialog,
        files,
    }
}