import avatar from "@/assets/images/album-thumb.png";

export const img404 = (e) => {
    e.target.src = avatar;
}
