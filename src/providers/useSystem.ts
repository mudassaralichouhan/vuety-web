import {inject, provide} from "vue";

const useSystem = Symbol("SYSTEM");
interface userSystemInterface {
  form: any
}
export function provideSystem(props: any) {
  provide(useSystem, props);
}

export function injectSystem(): any {
  const instance = inject(useSystem);

  if (!instance)
    throw new Error("Run provideSystem before injectSystem.");

  return instance;
}