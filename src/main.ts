import {createApp} from 'vue';
import App from './App.vue';
import router from '@/router';
import pinia from '@/plugins/pinia';
import cryptojs from '@/plugins/cryptojs';
import sweetalert2 from "@/plugins/sweetalert2";
import bootstrap from "@/plugins/bootstrap";
import Multiselect from "@/plugins/multiselect";
import VueFeather from "@/plugins/feather";

// Import Theme scss
import './assets/scss/app.scss';

const app: App<Element> = createApp(App);

app.use(bootstrap);
app.use(Multiselect);
app.use(VueFeather);
app.use(router);
app.use(pinia);
app.use(cryptojs);
app.use(sweetalert2);

app.mount('#app');
