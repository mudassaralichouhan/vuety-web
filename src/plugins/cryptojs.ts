import VueCryptojs from 'vue-cryptojs';
export default {
    install: (app, options) => {
        app.use(VueCryptojs);
    }
}