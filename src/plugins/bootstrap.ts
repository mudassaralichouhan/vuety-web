import {BootstrapVue3} from 'bootstrap-vue-3';

export default {
    install: (app, options) => {
        app.use(BootstrapVue3);
    }
}