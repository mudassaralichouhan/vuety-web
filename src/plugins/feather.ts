import VueFeather from 'vue-feather';

export default {
    install: (app, options) => {
        app.component(VueFeather.name, VueFeather);
    }
}