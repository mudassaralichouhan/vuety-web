import Multiselect from "@vueform/multiselect";

export default {
    install: (app, options) => {
        app.component(Multiselect.name, Multiselect);
    }
}