import axios, {type AxiosInstance} from "axios";
import {useSessionStore} from "@/stores/sessionStore";
import router from '@/router/index';

const apiClient = axios.create({
  baseURL: import.meta.env.VITE_BASE_URL,
});
apiClient.interceptors.request.use((config) => {
  config.headers.Authorization = `Bearer ${useSessionStore().getToken()}`;
  return config;
});
apiClient.interceptors.response.use(response => {
  return response;
}, async error => {

  if (error.response.status === 401) {
    const $state = useSessionStore();
    $state.reset();
    await router.push('/login');
  }

  return Promise.reject(error);
});

export const responseHandler = async (res: any): Promise<Array<string>> => {
  const resResult: Array<string> = [];

  if (res?.response?.data) {

    Object.keys(res.response.data).forEach((key: string) => {

      if (Array.isArray(res.response.data[key])) {
        res.response.data[key].forEach((value: string) => {
          resResult.push(value);
        });
        return;
      }

      if (key === 'message') {
        resResult.push(res.response.data[key]);
      } else {
        resResult.push(`${key}: ${res.response.data[key]}`);
      }
    });
  } else
    resResult.push(res?.message);

  return resResult;
}
export const api: AxiosInstance = axios.create({
  baseURL: import.meta.env.VITE_BASE_URL,
  headers: {
    "Content-type": "application/json",
  },
});
export default apiClient;