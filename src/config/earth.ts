import {api} from "@/config/api/api";
import {reactive} from "vue";

const countriesFetch = async () => {
  await api.get('/earth/countries')
    .then((response) => {
      earth.countries = response.data.map((c) => {
        return {value: c.id, label: c.name};
      });
    })
    .catch((e) => {
    });
}

export const statesFetch = async (id: number) => {
  if (id != 0) {
    await api.get(`/earth/countries/${id}/states`)
      .then((response) => {
        earth.states = response.data.map((s) => {
          return {value: s.id, label: s.name,}
        });
      })
      .catch((e) => {
      });
  }
}

export const citiesFetch = async (id: number) => {
  if (id != 0)
    await api.get(`/earth/state/${id}`)
      .then((response) => {
        earth.cities = response.data.cities.map((c) => {
          return {value: c.id, label: c.name,}
        });
      })
      .catch((e) => {
      });
}

countriesFetch();
export const earth = reactive({
  countries: [],
  states: [],
  cities: [],
});