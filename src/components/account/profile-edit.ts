import {useSessionStore} from "@/stores/sessionStore";
import {computed, reactive, ref, watch} from "vue";
import {helpers, maxLength, minLength, minValue, required, url} from "@vuelidate/validators";
import useVuelidate from "@vuelidate/core";
import apiClient, {responseHandler} from "@/config/api/api";
import {toast} from "vue3-toastify";
import {useFileDialog} from "@/composables/useFileDialog";


const {openDialog, files} = useFileDialog();

export const useSession = useSessionStore()?.getSetup()?.setup;
export const profileImage = ref(useSession?.photo);

export const profileForm = reactive({
  bio: "",
  password: "",
  website_url: "",
  photo: "",
});
export const userForm = reactive({
  company_name: "",
  name: useSession?.name,
  address: "",
  city_id: "0",
  postal_code: "",
  country_id: "0",
  state_id: "0",
  about_me: "",
});

export const profileState = reactive({
  loading: false,
  message: [],
});
export const userState = reactive({
  loading: false,
  message: [],
});

const profileRules = computed(() => {
  return {
    bio: {required, minLength: minLength(5), maxLength: maxLength(255)},
    website_url: {required, url},
  }
});
const userRules = computed(() => {
  return {
    company_name: {required, maxLength: maxLength(255)},
    name: {required, maxLength: maxLength(255)},
    address: {required, maxLength: maxLength(255)},
    city_id: {required, minValue: minValue(1)},
    postal_code: {
      required,
      isEmailExist: helpers.withMessage("Invalid Postal Code", (value: any) => {
        return /^\d{5}(-\d{4})?$/.test(value);
      })
    },
    country_id: {required, minValue: minValue(1)},
    state_id: {required, minValue: minValue(1)},
    about_me: {maxLength: maxLength(255)},
  }
});

apiClient.get(`profile`)
  .then(response => {
    profileState.loading = false;
    userState.loading = false;

    profileForm.bio = response.data.bio;
    profileForm.website_url = response.data.website_url;

    userForm.company_name = response.data.company_name;
    userForm.address = response.data.address;
    userForm.postal_code = response.data.postal_code;
    userForm.about_me = response.data.about_me;
    userForm.country_id = response.data.country_id;
    userForm.state_id = response.data.state_id;
    userForm.city_id = response.data.city_id;
  })
  .catch((e) => {
    profileState.loading = false;
    userState.loading = false;
    responseHandler(e).then(messages => {
      messages.forEach((m) => {
        toast(m, {
          autoClose: 3000,
        });
      })
    });
  });
export const $dpv = useVuelidate(profileRules, profileForm);
export const $userv = useVuelidate(userRules, userForm);

export const profileSubmit = async () => {
  const result = await $dpv.value.$validate();
  if (result) {

    profileState.loading = true;

    await apiClient.put(`profile`, profileForm)
      .then(response => {
        profileState.loading = false;
        toast(response.data?.message, {
          autoClose: 3000,
        });
      })
      .catch((e) => {
        profileState.loading = false;
        responseHandler(e).then(messages => {
          messages.forEach((m) => {
            toast(m, {
              autoClose: 3000,
            });
          })
        });
      });
  }
}
export const userSubmit = async () => {
  const result = await $userv.value.$validate();
  if (result) {

    userState.loading = true;

    await apiClient.put(`profile`, userForm)
      .then(response => {
        userState.loading = false;
        toast(response.data?.message, {
          autoClose: 3000,
        });
      })
      .catch((e) => {
        userState.loading = false;
        responseHandler(e).then(messages => {
          messages.forEach((m) => {
            toast(m, {
              autoClose: 3000,
            });
          })
        });
      });
  }
}
export const profilePhotoSubmit = async () => {
  openDialog();
}

watch(files, (files) => {

  apiClient.post(`photo`, {photo: files[0]}, {
    headers: {
      ...{'content-type': 'multipart/form-data'}
    }}).then(response => {

    profileImage.value = response.data.data;
    useSessionStore().setSetup({
      ...useSession,
      photo: response.data?.data,
    });

    files.value = null;

    toast(response.data?.message, {
      autoClose: 3000,
    });

  })
    .catch((e) => {

      responseHandler(e).then(messages => {
        messages.forEach((m) => {
          toast(m, {
            autoClose: 3000,
          });
        })
      });

    });
});