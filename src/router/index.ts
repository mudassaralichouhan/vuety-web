import {createRouter, createWebHistory} from 'vue-router'
import HomeView from '../views/HomeView.vue'
import _404 from '../views/errors/404.vue'
import GuestLayout from "../layouts/GuestLayout.vue";
import customer from "@/router/customer";
import admin from "@/router/admin";
import reseller from "@/router/reseller";
import {useSessionStore} from "@/stores/sessionStore";

const routesList = [
  {
    path: '/',
    name: 'home',
    meta: {layout: GuestLayout},
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    meta: {layout: GuestLayout},
    component: () => import('../views/AboutView.vue')
  },
  ...customer(),
  ...reseller(),
  ...admin(),
  {path: "/:pathMatch(.*)*", component: _404}
];
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: routesList
})
router.beforeEach(async (to: any) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = routesList.map((route) => {
    if (route?.meta?.auth === undefined)
      return route.path;
  });

  // all pages that have auth=true
  const authAllowPages = routesList.map((route) => {
    if (route?.meta?.auth === false || route?.meta?.auth === undefined)
      return route.path;
  });

  const noAuthAllowPages = routesList.map((route) => {
    if (route?.meta?.auth === false)
      return route.path;
  });

  if (Object.values(noAuthAllowPages).includes(to.path) && !!useSessionStore().getToken()) {
    return {
      path: "/dashboard"
    };
  }

  if (!Object.values(authAllowPages).includes(to.path) && !useSessionStore().getToken()) {
    return {
      path: '/login',
      query: {returnUrl: to.href}
    };
  }
});
export default router
