import auth from "./auth"
import AdminLayout from "@/layouts/AdminLayout.vue";

export default () => {
  return [
    ...auth(),
    {
      path: '/admin/dashboard',
      name: 'admin-dashboard',
      meta: {layout: AdminLayout, auth: true},
      component: () => import('@/views/admin/Dashboard.vue')
    }
  ];
};