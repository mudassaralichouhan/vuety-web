import AuthLayout from "@/layouts/AuthLayout.vue";
import CustomerLayout from "@/layouts/customer/CustomerLayout.vue";

export default () => {
  return [
    {
      path: '/login',
      name: 'login',
      meta: {layout: AuthLayout, auth: false},
      component: () => import('@/views/customer/auth/LoginView.vue')
    },
    {
      path: '/sign-up',
      name: 'sign-up',
      meta: {layout: AuthLayout, auth: false},
      component: () => import('@/views/customer/auth/RegisterView.vue')
    },
    {
      path: '/sign-up',
      name: 'sign-up',
      meta: {layout: AuthLayout, auth: false},
      component: () => import('@/views/customer/auth/RegisterView.vue')
    },
    {
      path: '/password/forgot',
      name: 'password-forgot',
      meta: {layout: AuthLayout, auth: false},
      component: () => import('@/views/customer/auth/ForgotView.vue')
    },
    {
      path: '/password/reset',
      name: 'password-reset',
      meta: {layout: AuthLayout, auth: false},
      component: () => import('@/views/customer/auth/ResetPasswordView.vue')
    },
    {
      path: '/email/resend',
      name: 'verification-email-resend',
      meta: {layout: AuthLayout, auth: false},
      component: () => import('@/views/customer/auth/RegisterVerificationResendView.vue')
    },
    {
      path: '/account',
      name: 'account',
      meta: {layout: CustomerLayout, auth: true},
      component: () => import('@/views/customer/auth/Account.vue')
    },
    {
      path: '/account',
      name: 'account',
      meta: {layout: CustomerLayout, auth: true},
      component: () => import('@/views/customer/auth/Account.vue')
    },
    {
      path: '/email-secondary-verified',
      name: 'email-secondary-verified',
      component: () => import('@/views/customer/auth/email/EmailSecondaryVerified.vue')
    },
    {
      path: '/email-primary-change-verified',
      name: 'email-primary-change-verified',
      component: () => import('@/views/customer/auth/email/EmailPrimaryChangeVerified.vue')
    },
  ];
};