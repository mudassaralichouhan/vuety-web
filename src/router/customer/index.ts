import auth from "./auth"

import CustomerLayout from "@/layouts/customer/CustomerLayout.vue";
export default () => {
  return [
    ...auth(),
    {
      path: '/dashboard',
      name: 'customer-dashboard',
      meta: {layout: CustomerLayout, auth: true},
      component: () => import('@/views/customer/DashboardView.vue')
    },
    {
      path: '/gallery/albums',
      name: 'customer-gallery',
      meta: {layout: CustomerLayout, auth: true},
      component: () => import('@/views/customer/gallery/AlbumsView.vue')
    },
    {
      path: '/gallery/album/:id',
      name: 'customer-gallery-album',
      meta: {layout: CustomerLayout, auth: true},
      component: () => import('@/views/customer/gallery/AlbumView.vue')
    },
    {
      path: '/gallery/photo/:id',
      name: 'customer-gallery-photo',
      meta: {layout: CustomerLayout, auth: true},
      component: () => import('@/views/customer/gallery/PhotoView.vue')
    },
    {
      path: '/gallery/photos',
      name: 'customer-gallery-photos',
      meta: {layout: CustomerLayout, auth: true},
      component: () => import('@/views/customer/gallery/PhotosView.vue')
    },
    {
      path: '/gallery/favorites',
      name: 'customer-gallery-favorite',
      meta: {layout: CustomerLayout, auth: true},
      component: () => import('@/views/customer/gallery/FavoriteView.vue')
    },
    {
      path: '/gallery/trash',
      name: 'customer-gallery-trash',
      meta: {layout: CustomerLayout, auth: true},
      component: () => import('@/views/customer/gallery/TrashView.vue')
    }
  ];
};