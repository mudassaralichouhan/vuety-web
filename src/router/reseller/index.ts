import auth from "./auth"
import ResellerLayout from "@/layouts/ResellerLayout.vue";

export default () => {
  return [
    ...auth(),
    {
      path: '/reseller/dashboard',
      name: 'reseller-dashboard',
      meta: {layout: ResellerLayout, auth: true},
      component: () => import('@/views/reseller/Dashboard.vue')
    }
  ];
};