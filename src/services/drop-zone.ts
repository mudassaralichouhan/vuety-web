import type {Ref} from "vue";
import {ref} from "vue";

export default function useDropZone() {
    const dragActive: Ref<boolean> = ref(false);
    const droppedFiles: Ref<FileList | null> = ref(null);
    const fileDataUrls = ref({});

    const toggleActive = (): void => {
        if (droppedFiles.value === null) {
            dragActive.value = !dragActive.value;
        }
    };

    const drop = (event: DragEvent): void => {
        event.preventDefault();
        const files: FileList | undefined = event.dataTransfer?.files;
        if (files) {
            droppedFiles.value = files;
            Array.from(files).forEach((file) => {
                getFileDataUrl(file);
            });
        }
    };

    const selectFiles = (event: Event): void => {
        const inputElement = event.target as HTMLInputElement;
        if (inputElement.files && inputElement.files.length > 0) {
            droppedFiles.value = inputElement.files;
            Array.from(inputElement.files).forEach((file) => {
                getFileDataUrl(file);
            });
        }
    };

    const clearDropped = (file: File): void => {
        if (droppedFiles.value !== null) {
            const updatedFiles = Array.from(droppedFiles.value).filter((f) => f !== file);
            if (updatedFiles.length > 0) {
                const dataTransfer = new DataTransfer();
                updatedFiles.forEach((f) => dataTransfer.items.add(f));
                droppedFiles.value = dataTransfer.files;
            } else {
                droppedFiles.value = null;
            }
        }
    };

    const getFileDataUrl = async (file: File) => {
        try {
            fileDataUrls.value[file.name] = await readDataUrl(file);
        } catch (error) {
            console.error('Error reading file:', error);
        }
    };

    const readDataUrl = (file: File) => {
        return new Promise<string>((resolve, reject) => {
            const reader = new FileReader();
            reader.onload = (event) => {
                resolve(event.target!.result as string);
            };
            reader.onerror = (error) => {
                reject(error);
            };
            reader.readAsDataURL(file);
        });
    };

    return {
        dragActive,
        droppedFiles,
        toggleActive,
        drop,
        selectFiles,
        clearDropped,
        getFileDataUrl,
        fileDataUrls,
    };
}
